$('#previewModal').on('show.bs.modal', function (e) {

    var username = $('#username');
    var email = $('#email');
    var description = $('#description');
    var image = $('#image').attr('src');

    var html = '<div class="alert alert-info">' +
        '<p><b>' + username.val() + '</b> (<a href="mailto:' + email + '">' + email.val() + '</a>)</p>';

    if (image) {
        html += '<p class="text-center"><img src="' + image + '"></p>';
    }

    html += '<p>' + description.val() + '</p></div>';

    $('#previewContent').html(html);

});
