<?php

namespace app\models;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class User
 * @package app\models
 */
class User
{
    public static $users = [
        'admin' => [
            'password' => '123',
            'name' => 'Site Admin',
        ],
    ];

    public function login(Request $request): bool
    {
        if (
            !empty(self::$users[$request->request->get('login')]) &&
            self::$users[$request->request->get('login')]['password'] === $request->request->get('password')
        ) {
            $session = new Session();
            $session->start();

            $session->set('userLogin', self::$users[$request->request->get('login')]);

            return true;
        }

        return false;
    }

    public function logout(): bool
    {
        $session = new Session();
        $session->start();
        $session->clear();

        return true;
    }
}
