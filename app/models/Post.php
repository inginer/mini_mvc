<?php

namespace app\models;

use ActiveRecord\Model;
use Eventviva\ImageResize;
use FileUpload\FileUploadFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Post
 * @package app\models
 */
class Post extends Model
{
    public static $validates_presence_of = [
        ['username', 'message' => 'Имя не может быть пустым!', 'on' => 'create'],
        ['email', 'message' => 'Email Не может быть пустым!', 'on' => 'create'],
        ['description', 'message' => 'Описание Не может быть пустым!', 'on' => 'create'],
    ];

    public static $table_name = 'post';

    public function uploadImage(Request $request)
    {
        if ($request->files->get('file')) {
            $factory = new FileUploadFactory(
                new \FileUpload\PathResolver\Simple(dirname(dirname(__DIR__)) . '/upload'),
                new \FileUpload\FileSystem\Simple(),
                [
                    new \FileUpload\Validator\MimeTypeValidator(['image/png', 'image/jpg', 'image/jpeg', 'image/gif']),
                    new \FileUpload\Validator\SizeValidator('2M'),
                ]
            );

            $instance = $factory->create($_FILES['file'], $request->server->all());
            $fileNameGenerator = new \FileUpload\FileNameGenerator\Simple();
            $instance->setFileNameGenerator($fileNameGenerator);

            $instance->addCallback('completed', function (\FileUpload\File $file) {

                $image = new ImageResize($file->getRealPath());
                $image->crop(320, 240, ImageResize::CROPCENTER);
                $image->save($file->getRealPath());

                $this->image = $file->getFilename();
                $this->save();

            });

            $instance->processAll();
        }
    }
}
