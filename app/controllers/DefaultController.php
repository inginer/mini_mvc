<?php

namespace app\controllers;

use app\models\Post;
use app\models\User;
use app\BaseController;
use JasonGrimes\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class DefaultController
 * @package app\controllers
 */
class DefaultController extends BaseController
{
    /**
     * @param Request $request
     * @return Response
     * @throws \ActiveRecord\RecordNotFound
     */
    public function actionIndex(Request $request)
    {
        $sortForPagination = '';

        if ($sort = $request->get('sort')) {
            $sortForPagination .= '&sort=' . $sort;
        }

        $paginator = new Paginator(Post::count(), 3, $request->get('page', 1), '?page=(:num)' . $sortForPagination);

        $posts = Post::find('all', [
            'limit' => 3,
            'offset' => ($request->get('page') ? $request->get('page', 1) - 1 : 0) * 3,
            'order' => $request->get('sort') ? $request->get('sort') . ' ASC' : 'id DESC',
        ]);

        return $this->render('default/index', [
            'posts' => $posts,
            'paginator' => $paginator,
        ]);
    }

    public function actionLogin(Request $request)
    {
        $model = new User();

        if ($request->isMethod('POST') && !$model->login($request)) {
            throw new \Exception('Login or password incorrect!');
        }

        return new RedirectResponse('/');
    }

    public function actionLogout(Request $request)
    {
        $model = new User();
        $model->logout();

        return new RedirectResponse('/');
    }
}
