<?php

namespace app\controllers;

use app\models\Post;
use app\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class TaskController
 * @package app\controllers
 */
class TaskController extends BaseController
{
    public function actionCreate(Request $request)
    {
        /** @var Post $model */
        $model = new Post();

        if ($request->isMethod('POST') && ($model = Post::create($request->request->all())) && $model->is_valid()) {
            $model->uploadImage($request);

            return new RedirectResponse('/');
        }

        return $this->render('task/form', [
            'model' => $model,
        ]);
    }

    public function actionUpdate(Request $request, int $id)
    {
        /** @var Post $model */
        $model = Post::find([$id]);

        if ($request->isMethod('POST') && $model->update_attributes($request->request->all()) && $model->is_valid()) {
            $model->uploadImage($request);
            return new RedirectResponse('/');
        }

        return $this->render('task/form', [
            'model' => $model,
        ]);
    }

    public function actionSetDone(Request $request, int $id)
    {
        /** @var Post $model */
        $model = Post::find([$id]);
        $model->is_done = true;
        $model->save();

        return new RedirectResponse($request->headers->get('referer'));
    }
}
