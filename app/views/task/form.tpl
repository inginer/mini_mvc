{extends file="../layout.tpl"}

{block name=body}
    <div class="page-header">
        <h1 class="page-title">
            Новая задача
        </h1>
    </div>
    {if $model->errors}
        <div class="alert alert-danger">
            <ul>
                {foreach from=$model->errors item=error}
                    <li>{$error}</li>
                {/foreach}
            </ul>
        </div>
    {/if}
    <form method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="username">Ваше имя</label>
                    <input type="text" value="{$model->username}" name="username" class="form-control" id="username"
                           placeholder="Имя">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Электронная почта</label>
                    <input type="text" value="{$model->email}" name="email" class="form-control" id="email"
                           placeholder="admin@example.com">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="description">Описание задачи</label>
                    <textarea name="description" class="form-control" id="description"
                              rows="12">{$model->description}</textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            {if $model->image}
                <p>
                    <img src="/upload/{$model->image}" id="image">
                </p>
            {/if}
            <label for="exampleInputFile">Выбрать изображения</label>
            <input type="file" name="file" id="exampleInputFile">
            <p class="help-block">Требования к изображениям - формат JPG/GIF/PNG, не более 320х240 пикселей.</p>
        </div>
        <button type="submit" class="btn btn-success">Сохранить</button>
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#previewModal">
            Предварительный просмотр
        </button>
    </form>
    <div class="modal fade" id="previewModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Предварительный просмотр</h4>
                </div>
                <div class="modal-body" id="previewContent">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>
{/block}
