{extends file="../layout.tpl"}

{block name=body}
    <div class="page-header">
        <h1 class="page-title">Список задач</h1>
    </div>
    <p>
        Сортировка по:
        [<a href="?sort=username">Имени пользователя</a>]
        [<a href="?sort=email">Email</a>]
        [<a href="?sort=is_done">Статус</a>]
    </p>
    {foreach from=$posts item=post}
        <div class="alert alert-{if $post->is_done}success{else}info{/if}">
            <p>
                <b>{$post->username}</b>
                (<a href="mailto:{$post->email}">{$post->email}</a>)
                [<a href="/task/update/{$post->id}">Редактировать</a>]
            </p>
            {if $post->image}
                <p class="text-center">
                    <img src="/upload/{$post->image}">
                </p>
            {/if}
            <p>{$post->description}</p>
            {if !empty($sessions.userLogin) and !$post->is_done}
                <p><a href="/task/set-done/{$post->id}">Пометить как выполненное</a></p>
            {/if}
        </div>
    {/foreach}
    {$paginator}
{/block}
