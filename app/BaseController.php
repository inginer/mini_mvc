<?php

namespace app;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class BaseController
 * @package app
 */
abstract class BaseController
{
    private $viewPath = __DIR__ . '/views/';
    private $compilePath = __DIR__ . '/../var/smarty';

    public function render(string $fileName, array $params = []): Response
    {
        $smarty = new \Smarty();
        $session = new Session();

        $smarty->setTemplateDir($this->viewPath);
        $smarty->setCompileDir($this->compilePath);
        $smarty->assign('sessions', $session->all());
        $smarty->assign($params);

        $responseHtml = $smarty->fetch($fileName . '.tpl');

        return new Response($responseHtml);
    }
}
