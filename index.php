<?php
require __DIR__ . '/vendor/autoload.php';

ActiveRecord\Config::initialize(function ($cfg) {
    $cfg->set_model_directory(__DIR__ . '/app/models');
    $cfg->set_connections(
        array(
            'production' => 'mysql://root:12150506@localhost/bee_jee;charset=utf8'
        )
    );
    $cfg->set_default_connection('production');
});

$routes = new Symfony\Component\Routing\RouteCollection();

$routes->add('homepage', new Symfony\Component\Routing\Route('/', [
    '_controller' => [
        new \app\controllers\DefaultController(),
        'actionIndex',
    ],
]));

$routes->add('login', new Symfony\Component\Routing\Route('/login', [
    '_controller' => [
        new \app\controllers\DefaultController(),
        'actionLogin',
    ],
]));
$routes->add('logout', new Symfony\Component\Routing\Route('/logout', [
    '_controller' => [
        new \app\controllers\DefaultController(),
        'actionLogout',
    ],
]));

$routes->add('task.create', new Symfony\Component\Routing\Route('/task/create', [
    '_controller' => [
        new \app\controllers\TaskController(),
        'actionCreate',
    ],
]));

$routes->add('task.update', new Symfony\Component\Routing\Route('/task/update/{id}', [
    '_controller' => [
        new \app\controllers\TaskController(),
        'actionUpdate',
    ],
]));

$routes->add('task.set.done', new Symfony\Component\Routing\Route('/task/set-done/{id}', [
    '_controller' => [
        new \app\controllers\TaskController(),
        'actionSetDone',
    ],
]));

$core = new \app\Core();
$core->init($routes);
